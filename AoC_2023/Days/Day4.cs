﻿
using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2023.Days;
internal sealed class Day4
{
    public void ScratchCards()
    {
        var inputReal = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d4i.txt");
        var inputTest = @"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";

        var gameCards = inputReal;// inputTest.Split("\n");
        var points = 0;
        var cards = 196;

        for (int i = 0; i < gameCards.Length; i++)
        {
            var cardParts = gameCards[i].Split(":");
            var numbers = cardParts[1].Split("|");
            var winningNumbers = numbers[0].Trim().Split();
            var cardNumbers = numbers[1].Trim().Split();

            var matches = cardNumbers.Intersect(winningNumbers).Where(x => x != "");

            if (matches.Any()) cards += CountCardCopies(gameCards, i, matches.Count());
            points += (int)Math.Pow(2, matches.Count() - 1);
        }

        Console.WriteLine("Total points worth: " + points);
        Console.WriteLine("Total cards: " + cards);
    }

    private int CountCardCopies(string[] gameCards, int currentIndex, int copies)
    {
        var cards = 0;

        for (int i = currentIndex + 1; i < currentIndex + 1 + copies; i++)
        {
            cards++;
            var parts = gameCards[i].Split(":");
            var nums = parts[1].Split("|");
            var winningNumbers = nums[0].Trim().Split();
            var cardNumbers = nums[1].Trim().Split();

            var matches = cardNumbers.Intersect(winningNumbers).Where(x => x != "");
                
            if(matches.Any()) cards += CountCardCopies(gameCards, i, matches.Count());  
        }

        return cards;
    }
}
