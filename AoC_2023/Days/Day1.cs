﻿
using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text.RegularExpressions;

namespace AoC_2023.Days;
public class Day1
{
    public void CalibrationValues()
    {
        var inputs = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d1p1.txt");

        var intList = new List<int>();

        foreach (var input in inputs)
        {
            var modifiedInput = Regex.Replace(input, "one", "o1e");
            modifiedInput = Regex.Replace(modifiedInput, "two", "t2o");
            modifiedInput = Regex.Replace(modifiedInput, "three", "th3ee");
            modifiedInput = Regex.Replace(modifiedInput, "four", "fo4r");
            modifiedInput = Regex.Replace(modifiedInput, "five", "fi5e");
            modifiedInput = Regex.Replace(modifiedInput, "six", "s6x");
            modifiedInput = Regex.Replace(modifiedInput, "seven", "se7en");
            modifiedInput = Regex.Replace(modifiedInput, "eight", "ei8ht");
            modifiedInput = Regex.Replace(modifiedInput, "nine", "ni9e");

            var pattern = @"(?:[1-9])";

            MatchCollection matches = Regex.Matches(modifiedInput, pattern, RegexOptions.IgnoreCase);

            var firstNumber = matches[0].Value;
            var lastNumber = matches[matches.Count - 1].Value;

            Console.WriteLine($"({firstNumber + lastNumber})");
            intList.Add(int.Parse(firstNumber + lastNumber));
        }

        Console.WriteLine("Sum: " + intList.Sum());
    }
}
