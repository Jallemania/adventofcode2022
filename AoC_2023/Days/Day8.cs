﻿

using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2023.Days;
internal class Day8
{
    public void HauntedWasteland()
    {
        var input = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d8i.txt");
        var inputTest = new string[]
                            {
                                "RL",
                                "",
                                "AAA = (BBB, CCC)",
                                "BBB = (DDD, EEE)",
                                "CCC = (ZZZ, GGG)",
                                "DDD = (DDD, DDD)",
                                "EEE = (EEE, EEE)",
                                "GGG = (GGG, GGG)",
                                "ZZZ = (ZZZ, ZZZ)"
                            };
        var inputTest2 = new string[]
                            {
                                "LR",
                                "",
                                "11A = (11B, XXX)",
                                "11B = (XXX, 11Z)",
                                "11Z = (11B, XXX)",
                                "22A = (22B, XXX)",
                                "22B = (22C, 22C)",
                                "22C = (22Z, 22Z)",
                                "22Z = (22B, 22B)",
                                "XXX = (XXX, XXX)"
                            };

        var dingus = input;
        var directions = dingus[0];
        var paths = dingus.Skip(2).ToArray();
        var currentPos = "AAA";
        int steps = 0;
        var keepAlive = true;

        while (keepAlive)
        {
            foreach (var dir in directions)
            {
                var path = paths.First(x => x.StartsWith(currentPos));

                if (dir == 'L')
                {
                    currentPos = path.Substring(7, 3);
                }
                else if (dir == 'R')
                {
                    currentPos = path.Substring(12, 3);
                }

                steps++;

                if (currentPos == "ZZZ")
                {
                    keepAlive = false;
                    break;
                }
            }
        }

        Console.WriteLine("Steps: " + steps);

        PartTwo(dingus);
    }

#region
    private void PartTwo(string[] dingus)
    {
        var directions = dingus[0];
        var paths = dingus.Skip(2).ToArray();
        var startingPaths = paths.Select(p => p.Substring(0,3)).Where(p => p.Length > 2 && p.Substring(2, 1) == "A").ToList();
        int steps = 0;
        var keepAlive = true;

        while (keepAlive)
        {
            var prev = new int[startingPaths.Count, 3];
            foreach (var dir in directions)
            {
                for(int i = 0; i < startingPaths.Count; i++)
                {
                    var path = paths.First(p => p.StartsWith(startingPaths[i]));
                    if (dir == 'L')
                    {
                        startingPaths[i] = path.Substring(7, 3);
                    }
                    else if (dir == 'R')
                    {
                        startingPaths[i] = path.Substring(12, 3);
                    }
                }

                steps++;

                if (startingPaths.TrueForAll(p => p.Substring(2, 1) == "Z"))
                {
                    keepAlive = false;
                    break;
                }
            }
        }

        Console.WriteLine("Steps PT.II: " + steps);
    }

    static int GCD(int a, int b)
    {
        while (b != 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    // Function to calculate LCM
    static int LCM(int a, int b)
    {
        // Avoid division by zero
        if (a == 0 || b == 0)
        {
            return 0;
        }

        // Calculate LCM using the formula: LCM(a, b) = |a * b| / GCD(a, b)
        return Math.Abs(a * b) / GCD(a, b);
    }
#endregion 
}
