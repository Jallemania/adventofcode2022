﻿
using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text.RegularExpressions;

namespace AoC_2023.Days;
internal sealed class Day3
{
    public void GearRatios()
    {
        var gridLines = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d3i.txt");

        string numPattern = @"\b\d+\b";
        var symPattern = @"[^\w\s\d.]";

        var engineParts = new List<int>();
        var partsSum = 0;

        for (int i = 1; i < gridLines.Length - 1; i++)
        {
            var lineParts = new List<int>();

            MatchCollection numTop = Regex.Matches(gridLines[i - 1], numPattern);
            MatchCollection numMiddle = Regex.Matches(gridLines[i], numPattern);
            MatchCollection numBottom = Regex.Matches(gridLines[i + 1], numPattern);

            MatchCollection symMatches = Regex.Matches(gridLines[i], symPattern);

            foreach (Match sym in symMatches)
            {
                var ratios = new List<int>();
                if (sym.Value == "*")
                {
                    foreach (Match num in numTop)
                    {
                        if (sym.Index == num.Index || sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) ratios.Add(int.Parse(num.Value));
                    }
                    foreach (Match num in numMiddle)
                    {
                        if (sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) ratios.Add(int.Parse(num.Value));
                    }
                    foreach (Match num in numBottom)
                    {
                        if (sym.Index == num.Index || sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) ratios.Add(int.Parse(num.Value));
                    }
                }
                else
                {
                    foreach (Match num in numTop)
                    {
                        if (sym.Index == num.Index || sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) lineParts.Add(int.Parse(num.Value));
                    }
                    foreach (Match num in numMiddle)
                    {
                        if (sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) lineParts.Add(int.Parse(num.Value));
                    }
                    foreach (Match num in numBottom)
                    {
                        if (sym.Index == num.Index || sym.Index == num.Index - 1 || sym.Index == num.Index + 1 || sym.Index == num.Index + 2) ratios.Add(int.Parse(num.Value));
                        else if (num.Length == 3 && sym.Index == num.Index + 3) lineParts.Add(int.Parse(num.Value));
                    }
                }

                partsSum += lineParts.Sum();

                if (ratios.Count == 2) 
                {
                    engineParts.Add(ratios[0] * ratios[1]);
                }
            }
        }

        Console.WriteLine(partsSum);

        Console.WriteLine(engineParts.Sum());
    }
}
