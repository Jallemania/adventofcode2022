﻿
using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2023.Days;
internal sealed class Day5
{
    public void IFYouGiveASeedAFertilizer()
    {
        var inputTest = @"seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";
        var inputReal = ReadFromFileStatic.GetText(PathWays.TwentyThree + "d5i.txt");

        var almanac = inputTest.Split("\r\n\r\n");
        var seeds = almanac[0].Split(" ");
        var seed2Soil = almanac[1].Split("\r\n");
        var soil2Fertilizer = almanac[2].Split("\r\n");
        var fertilizer2Water = almanac[3].Split("\r\n");
        var water2Light = almanac[4].Split("\r\n");
        var light2Temp = almanac[5].Split("\r\n");
        var temp2Humidity = almanac[6].Split("\r\n");
        var humidity2location = almanac[7].Split("\r\n");
    }
}
