﻿

using System.Numerics;

namespace AoC_2023.Days;
internal class Day6
{
    public void Wait4It()
    {
        var testInput = @"Time:      7  15   30
Distance:  9  40  200";
        var realInput = @"Time:        56     97     77     93
Distance:   499   2210   1097   1440";

        var parts = realInput.Split("\r\n");

        //Part1
        //var times = parts[0].Split(" ").Select(d => d).Where(d => d != "").ToList();
        //var distances = parts[1].Split(" ").Select(d => d).Where(d => d != "").ToList();

        //Part2
        var times = parts[0].Split(":");
        var distances = parts[1].Split(":");
        times[1] = times[1].Replace(" ", "");
        distances[1] = distances[1].Replace(" ", "");

        var winnableHoldDuration = new List<BigInteger>();

        for (int i = 1; i < times.Count(); i++)
        {

            var time = BigInteger.Parse(times[i]);
            var recordDistance = BigInteger.Parse(distances[i]);

            BigInteger ways2Win = 0;

            for (int j = 1; j < time; j++) 
            {
                var distance = (time - j) * j;

                if(distance > recordDistance) ways2Win++;
            }

            winnableHoldDuration.Add(ways2Win);
        }


        var product = winnableHoldDuration.Aggregate((result, number) => result * number);
        
        Console.WriteLine("Ways to win product: " + product);
    }
}
