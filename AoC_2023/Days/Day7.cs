﻿
using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text.RegularExpressions;

namespace AoC_2023.Days;
internal class Day7
{
    public void CamelCards()
    {
        var inputReal = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d7i.txt");
        var inputTest = new string[]
            {
                "32T3K 765",
                "T55J5 684",
                "KK677 28",
                "KTJJT 220",
                "QQQJA 483"
            };
        var pattern = "[1-9AKQJT]";
        string fiveOfAKindPattern = @"(.)\1{4}";
        string fourOfAKindPattern = @"(.)\1{3}";
        string threeOfAKindPattern = @"(.)\1{2}";
        string twoPairPattern = @"((.)\2{1}(.)\3{1}|(.)\4{1}(.)\5{1})";
        string onePairPattern = @"(.)\1{1}";
        string fullHousePattern = @"((.)\2{2}(.)\3{1}|(.)\4{1}(.)\5{2})";
        var combo = new List<KeyValuePair<int, int>>();


        for (int i = 0; i < inputTest.Length; i++)
        {
            var parts = inputTest[i].Split(" ");
            var bid = int.Parse(parts[1]);

            var hand = Regex.Matches(parts[0], pattern);
            var handValue = 0;

            if (CheckPattern(parts[0], fiveOfAKindPattern, "Five of a kind")) ;
            if (CheckPattern(parts[0], fourOfAKindPattern, "Four of a kind")) ;
            if (CheckPattern(parts[0], threeOfAKindPattern, "Three of a kind")) ;
            if (CheckPattern(parts[0], twoPairPattern, "Two pair")) ;
            if (CheckPattern(parts[0], onePairPattern, "One pair")) ;
            if (CheckPattern(parts[0], fullHousePattern, "Full house")) ;

            combo.Add(new KeyValuePair<int, int>(handValue, bid));
            combo.Sort((x, y) => x.Key.CompareTo(y.Key));


        }

        foreach (var item in combo)
        {
            Console.WriteLine(item.Key);
        }
    }
    static bool CheckPattern(string card, string pattern, string label)
    {
        if (Regex.IsMatch(card, pattern))
        {
            Console.WriteLine(label);
            return true;
        }
        else
        {
            return false;
        }
    }
}
