﻿
using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text.RegularExpressions;

namespace AoC_2023.Days;
internal sealed class Day2
{
    public void CubeConundrum()
    {
        var games = ReadFromFileStatic.GetTextLines(PathWays.TwentyThree + "d2i.txt");

        var maxReds = 12;
        var maxGreens = 13;
        var maxBlues = 14;

        var possibleGames = new List<int>();
        var powerOfSets = new List<int>();

        for (int i = 0; i < 100; i++)
        {
            var highestRed = 0; 
            var highestGreen = 0; 
            var highestBlue = 0;

            var gameId = games[i].Substring(0, 8);
            var game = games[i].Substring(7);
            var gameSets = game.Split(";");

            var isPossible = true;

            for (int j = 0; j < gameSets.Length; j++)
            {
                var pattern = @"\b(?:green|blue|red|[1-9]|[1-9][0-9])\b";
                MatchCollection matches = Regex.Matches(gameSets[j], pattern, RegexOptions.IgnoreCase);

                for (int k = 0; k < matches.Count; k++)
                {
                    if (char.IsDigit(matches[k].Value, 0))
                    {
                        var amount = int.Parse(matches[k].Value);

                        switch (matches[k + 1].Value)
                        {
                            case "red":
                                if (amount > maxReds) { isPossible = false; Console.WriteLine($"IMPOSSIBLE: {gameId} Red = {amount}"); }
                                if (amount > highestRed) highestRed = amount;
                                break;
                            case "blue":
                                if (amount > maxBlues) { isPossible = false; Console.WriteLine($"IMPOSSIBLE: {gameId} Blue = {amount}"); }
                                if (amount > highestBlue) highestBlue = amount;
                                break;
                            case "green":
                                if (amount > maxGreens) { isPossible = false; Console.WriteLine($"IMPOSSIBLE: {gameId} Green = {amount}"); }
                                if (amount > highestGreen) highestGreen = amount;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            if (isPossible)
            {
                var pattern = @"\b(?:[1-9]|[1-9][0-9]|100)\b";
                var match = Regex.Match(gameId, pattern, RegexOptions.IgnoreCase);
                
                possibleGames.Add(int.Parse(match.Value));
            }

            var powerOfGame = highestRed * highestBlue * highestGreen;
            powerOfSets.Add(powerOfGame);

        }

        Console.WriteLine("IDs Sum: " + possibleGames.Sum());
        Console.WriteLine("Power of Sum: " + powerOfSets.Sum());
    }

}
