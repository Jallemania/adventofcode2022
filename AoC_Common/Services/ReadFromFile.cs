﻿
namespace AoC_Common.Services;

public class ReadFromFile
{
    public string GetText(string path)
    {
        // Read the file as one string.
        return System.IO.File.ReadAllText(@path);

    }

    public string[] GetTextLines(string path)
    {
        // Read each line of the file into a string array. Each element
        // of the array is one line of the file.
        return System.IO.File.ReadAllLines(@path); 
    }
}

public static class ReadFromFileStatic
{
    /// <summary>
    ///  Read the file as one string.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string GetText(string path)
    {
        return System.IO.File.ReadAllText(@path);

    }

    /// <summary>
    ///   Read each line of the file into a string array. Each element
    ///   of the array is one line of the file.
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string[] GetTextLines(string path)
    {
        return System.IO.File.ReadAllLines(@path);
    }

    /// <summary>
    ///   Gets 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static char[,] GetTextAs2dArray(string path)
    {
        var stringArray = System.IO.File.ReadAllLines(@path);
        int rows = stringArray.Length;
        int cols = stringArray[0].Length; // Assuming all strings have the same length
        char[,] char2DArray = new char[rows, cols];

        // Fill the 2D array
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                char2DArray[i, j] = stringArray[i][j];
            }
        }
        return char2DArray;
    }
}

