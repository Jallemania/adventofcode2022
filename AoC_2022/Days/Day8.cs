﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day8
    {
        private readonly ReadFromFile fileReader = new();
        public void TreeTopHouse()
        {            
            string[] input = fileReader.GetTextLines(PathWays.TwentyTwo + "Day8Input.txt");
            int[,] grid = new int[input[0].Length, input.Length];

            int i = 0;

            foreach (var line in input)
            {
                int j = 0;
                foreach (var num in line)
                {
                    grid[i, j] = int.Parse(num.ToString());
                    j++;
                }
                i++;
            }

            Console.WriteLine($"visible trees: {GetVisibleTrees(grid)}");
            Console.WriteLine($"Highest score: {GetHighestScenicScore(grid)}");
            
        }

        private int GetHighestScenicScore(int[,] grid)
        {
            int gridLength = grid.GetLength(0);
            int gridHeight = grid.GetUpperBound(0);
            int score = 0;

            for (int i = 1; i < gridHeight; i++)
            {
                for (int j = 1; j < gridLength - 1; j++)
                {
                    int b = 0;
                    int r = 0;
                    int t = 0;
                    int l = 0;

                    for (int k = j + 1; k < gridLength; k++)
                    {
                        if (grid[i, j] > grid[i, k]) r++;
                        else if (grid[i, j] <= grid[i, k]) { r++; break; }
                    }
                    for (int k = i + 1; k < gridLength; k++)
                    {
                        if (grid[i, j] > grid[k, j]) b++;
                        else if (grid[i, j] <= grid[k, j]) { b++; break; }
                    }
                    for (int k = j - 1; k >= 0; k--)
                    {
                        if (grid[i, j] > grid[i, k]) l++;
                        else if (grid[i, j] <= grid[i, k]) { l++; break; }
                    }
                    for (int k = i - 1; k >= 0; k--)
                    {
                        if (grid[i, j] > grid[k, j]) t++;
                        else if (grid[i, j] <= grid[k, j]) { t++; break; }
                    }

                    if ((t * l * b * r) > score)
                    {
                        score = t*r*b*l;
                    }
                }
            }

            return score;
        }

        private int GetVisibleTrees(int[,] grid)
        {
            int gridLength = grid.GetLength(0);
            int gridHeight = grid.GetUpperBound(0) + 1;
            int visibleTrees = (gridHeight * 2) + (gridLength * 2) - 4;

            for (int i = 1; i < gridHeight - 1; i++)
            {
                for (int j = 1; j < gridLength - 1; j++)
                {
                    bool b = true;
                    bool r = true;
                    bool t = true;
                    bool l = true;

                    for (int k = j + 1; k < gridLength; k++)
                    {
                        if (grid[i, j] <= grid[i, k]) r = false;
                    }
                    for (int k = i + 1; k < gridHeight; k++)
                    {                        
                        if (grid[i, j] <= grid[k, j]) b = false;
                    }
                    for (int k = j - 1; k >= 0; k--)
                    {
                        if (grid[i, j] <= grid[i, k]) l = false;
                    }
                    for (int k = i - 1; k >= 0; k--)
                    {                   
                        if (grid[i, j] <= grid[k, j]) t = false;
                    }

                    if (b || r || l || t) {
                        visibleTrees++;
                    } 
                }
            }

            return visibleTrees;
        }
    }
}
