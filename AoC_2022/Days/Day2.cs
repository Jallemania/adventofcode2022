﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day2
    {
        readonly ReadFromFile fileReader = new();
        private const string path = PathWays.TwentyTwo + "Day2Input.txt"; 

        public void RockPaperScissor()
        {
            int totalScore = 0;
            string[] lines = fileReader.GetTextLines(path);

            for (int i = 0; i < lines.Length; i++)
            {
                lines[i] = lines[i].Replace("A", "rock").Replace("X", "loss").Replace("B", "paper").Replace("Y", "draw").Replace("C", "scissor").Replace("Z", "win");

                var splitLine = lines[i].Split(' ');

                totalScore += GetScore(splitLine[0], splitLine[1]);
            }

            Console.WriteLine("Your total score is: " + totalScore);
        }

        private int GetScore(string opponentHand, string outcome)
        {
            int score = 0;

            if (outcome == "loss") score += 0;
            else if (outcome == "draw") score += 3;
            else if (outcome == "win") score += 6;

            string myHand = GetMyHand(opponentHand, outcome);

            if (myHand == "rock") score += 1;
            else if (myHand == "paper") score += 2;
            else if (myHand == "scissor") score += 3;

            return score;
        }

        private string GetMyHand(string opponentHand, string outcome)
        {
            string myHand = "";

            if (opponentHand == "rock")
            {
                if (outcome == "draw") myHand = "rock";
                else if (outcome == "win") myHand = "paper";
                else if (outcome == "loss") myHand = "scissor";
            } 
            else if (opponentHand == "paper")
            {
                if (outcome == "loss") myHand = "rock";
                else if (outcome == "draw") myHand = "paper";
                else if (outcome == "win") myHand = "scissor";
            }
            else if(opponentHand == "scissor")
            {
                if (outcome == "win") myHand = "rock";
                else if (outcome == "loss") myHand = "paper";
                else if (outcome == "draw") myHand = "scissor";
            }

            return myHand;
        }
    }
}
