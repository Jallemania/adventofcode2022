﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day5
    {
        private readonly ReadFromFile fileReader = new();

        public void SupplyStacks(bool runFirst)
        {
            string type = runFirst ? "crates" : "stacks";

            string[] lines = fileReader.GetTextLines(PathWays.TwentyTwo + "Day5Input.txt");
            Stack<string>[] stacks = GetStacks(lines);
            string[] moves = GetMoves(lines);

            foreach (var move in moves)
            {
                int.TryParse(move.Substring(5,2), out int crates);
                int.TryParse(move.Substring(move.Length - 6, 1), out int moveFrom);
                int.TryParse(move.Substring(move.Length - 1), out int moveTo);

                if (runFirst)
                {
                    stacks = MoveSingleCrates(crates, moveFrom, moveTo, stacks);

                }
                else
                {
                    stacks = MoveStacks(crates, moveFrom, moveTo, stacks);
                }
            }


            Console.WriteLine($"when {type} are moved: " + stacks[0].Peek() + stacks[1].Peek() + stacks[2].Peek() + stacks[3].Peek() + stacks[4].Peek() + stacks[5].Peek() + stacks[6].Peek() + stacks[7].Peek() + stacks[8].Peek());
        }

        private Stack<string>[] GetStacks(string[] lines)
        {
            Stack<string>[] stacks = new Stack<string>[9]
            {
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>(),
                new Stack<string>()
            };

            for (int i = 7; i >= 0; i--)
            {
               int index = 0;
                
                for (int j = 1; j < lines[i].Length; j += 4)
                {
                    string character = lines[i].Substring(j, 1);
                    if (character != " ") stacks[index].Push(character);
                    index++;
                }
            }

            return stacks;
            
        }

        private string[] GetMoves(string[] lines)
        {
            List<string> moves = new List<string>();
            for (int i = 10; i < lines.Length; i++)
            {
                moves.Add(lines[i]);
            }

            return moves.ToArray();
        }

        private Stack<string>[] MoveStacks(int crates, int moveFrom, int moveTo, Stack<string>[] stacks)
        {
            List<string> stackToMove = new();

            for (int i = 0; i < crates; i++)
            {
                stackToMove.Add(stacks[moveFrom - 1].Peek());
                stacks[moveFrom - 1].Pop();
            }

            stackToMove.Reverse();

            foreach (var crate in stackToMove)
            {
                stacks[moveTo - 1].Push(crate);
            }

            return stacks;
        }

        private Stack<string>[] MoveSingleCrates(int crates, int moveFrom, int moveTo, Stack<string>[] stacks)
        {
            for (int i = 0; i < crates; i++)
            {
                stacks[moveTo - 1].Push(stacks[moveFrom - 1].Peek());
                stacks[moveFrom - 1].Pop();
            }

            return stacks;
        }
    }
}
