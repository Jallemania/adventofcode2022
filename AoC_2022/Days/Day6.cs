﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day6
    {
        private readonly ReadFromFile fileReader = new();

        public void TuningTrouble()
        {
            string signal = fileReader.GetText(PathWays.TwentyTwo + "Day6Input.txt");

            int firstMarker = GetFirstStartOfPacket(signal);
            int firstMessage = GetFirstStartOfMessage(signal);

            Console.WriteLine($"the first marker appears after {firstMarker} characters ");
            Console.WriteLine($"The fist message marker appears after {firstMessage} characters");
        }

        private int GetFirstStartOfMessage(string signal)
        {
            for (int i = 13; i < signal.Length; i++)
            {
                string part = signal.Substring(i - 13, 14);

                if (uniqueCharacters(part))
                {
                    return (i + 1);
                }
            }

            return - 1;
        }

        private int GetFirstStartOfPacket(string signal)
        {
            for (int i = 3; i < signal.Length; i++)
            {
                char a = signal[i];
                char b = signal[i-1];
                char c = signal[i-2];
                char d = signal[i-3];

                if (a != b && a != c && a != d && b != c && b != d && c != d)
                {
                    return (i + 1);
                }
            }

            return -1;
        }

        static private bool uniqueCharacters(string str)
        {
            for (int i = 0; i < str.Length; i++)
                for (int j = i + 1; j < str.Length; j++)
                    if (str[i] == str[j])
                        return false;

            return true;
        }
    }
}
