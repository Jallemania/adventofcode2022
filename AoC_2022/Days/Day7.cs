﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day7
    {
        private readonly ReadFromFile fileReader = new();

        public void SpaceSaver()
        {
            string[] actions = fileReader.GetTextLines(PathWays.TwentyTwo + "Day7Input.txt");

            Dictionary<string, int> dirSizes = new();
            Stack<string> dirPath = new();


            foreach (var line in actions)
            {
                string[] parts = line.Split(' ');

                switch (parts)
                {
                    case ["$", "cd", ".."]:
                        dirPath.Pop();
                        break;
                    case ["$", "cd", var dir]:
                        dirPath.Push(dir);
                        break;
                    case ["$", "ls"]:
                        break;
                    case ["dir", ..]:
                        break;
                    case [var fSize, _]:
                        int fSizeInt = int.Parse(fSize);
                        for (int i = 0; i < dirPath.Count; i++)
                        {
                            string dir = string.Join("-", dirPath.Reverse().Take(i + 1));
                            if (!dirSizes.ContainsKey(dir))
                            {
                                dirSizes[dir] = fSizeInt;
                            }
                            else
                            {
                                dirSizes[dir] = dirSizes[dir] + fSizeInt;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            object sum = dirSizes.Where(dir => dir.Value < 100000).Sum(dir => dir.Value);

            dirPath.Push("/");
            int requiredSpace = 30000000 - (70000000 - dirSizes["/"]);
            object smallestDir = dirSizes.OrderBy(dir => dir.Value).FirstOrDefault(dir => dir.Value > requiredSpace).Value;

            Console.WriteLine($"Sum of the total directory sizes: {sum} \nTotal size of smallest possible directory: {smallestDir}");
        }
    }       
}
