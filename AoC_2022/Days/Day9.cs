﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day9
    {
        private readonly ReadFromFile fileReader = new();

        public void RopeSim()
        {
            string[] input = fileReader.GetTextLines(PathWays.TwentyTwo + "Day9Input.txt");
            /*string[] input = new string[8]
            {
                "R 4",
                "U 4",
                "L 3",
                "D 1",
                "R 4",
                "D 1",
                "L 5",
                "R 2"
            };*/
              
            List<string[]> moves = new List<string[]>();
            int l = 0;
            int r = 0;
            int u = 0;
            int d = 0;


            foreach (string line in input)
            {
                string[] move = line.Split(' ');
                moves.Add(move);

                switch (move)
                {
                    case [string direction, string steps]:
                        if(direction == "L") l -= int.Parse(steps);
                        else if(direction == "R") r += int.Parse(steps);
                        else if (direction == "U") u -= int.Parse(steps);
                        else if (direction == "D") d += int.Parse(steps);
                        break;
                    default:
                        break;
                }
            }

            int arrWidth = 0;
            int arrHeight = d + u;

            int startW = 1000;
            int startH = 1000;

            bool[,] grid = new bool[2500, 2500];

            int visitedNodes = GetVisitedNodes(moves, grid, startH, startW);

            Console.WriteLine($"The tail has been to {visitedNodes} nodes");
        }

        private int GetVisitedNodes(List<string[]> moves, bool[,] grid, int startH, int startW)
        {
            int visitedNodes = 0;
            int posH = startH - 1;
            int posV = startW - 1;
            string lastDir = moves[0][0];

            foreach (var move in moves)
            {
                switch (move[0])
                {
                    case "R":
                        for (int i = 0; i < int.Parse(move[1]); i++)
                        {
                            if (lastDir == move[0]) grid[posH, posV] = true;
                            else lastDir = move[0];
                            posV++;
                        }
                        break;
                    case "L":
                        for (int i = 0; i < int.Parse(move[1]); i++)
                        {
                            if (lastDir == move[0]) grid[posH, posV] = true;
                            else lastDir = move[0];
                            posV--;
                        }
                        break;
                    case "U":
                        for (int i = 0; i < int.Parse(move[1]); i++)
                        {
                            if (lastDir == move[0]) grid[posH, posV] = true;
                            else lastDir = move[0];
                            posH--;
                        }
                        break;
                    case "D":
                        for (int i = 0; i < int.Parse(move[1]); i++)
                        {
                            if (lastDir == move[0]) grid[posH, posV] = true;
                            else lastDir = move[0];
                            posH++;
                            
                        }
                        break;
                    default:
                        break;
                }
            }

            for (int k = 0; k < grid.GetUpperBound(0); k++)
            {
                for (int i = 0; i < grid.GetLength(0); i++)
                {
                    if (grid[i, k]) Console.Write("#");
                    else Console.Write(".");
                }
                Console.Write("\n");
            }
           

            foreach (bool visited in grid)
            {
                if (visited)
                {
                    visitedNodes++;
                }
            }

            return visitedNodes;    
        }
    }
}
