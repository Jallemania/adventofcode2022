﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day3
    {
        private readonly ReadFromFile fileReader = new();
        private readonly string itemValues = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public void RucksackReorganization()
        {
            string[] rucksacks = fileReader.GetTextLines(PathWays.TwentyTwo + "Day3Input.txt");
            int totalRucksackSum = 0;
            int groupTotalSum = GetGroupTotal(rucksacks);

            foreach (var rucksack in rucksacks)
            {
                string compartment1 = rucksack.Substring(0, (rucksack.Length / 2));
                string compartment2 = rucksack.Substring(rucksack.Length / 2);

                char commonChar = GetCommonChar(compartment1, compartment2);

                totalRucksackSum += GetCharValue(commonChar);
            }


            Console.WriteLine("The priority sum is: " + totalRucksackSum);
            Console.WriteLine("The group priority sum is: " + groupTotalSum);
        }

        private int GetGroupTotal(string[] rucksacks)
        {
            int totalSum = 0;

            for (int i = 2; i < rucksacks.Length; i += 3)
            {
                char commonChar = GetGroupCommonChar(rucksacks[i - 2], rucksacks[i - 1], rucksacks[i]);
                totalSum += GetCharValue(commonChar);
            }

            return totalSum;
        }

        private char GetGroupCommonChar(string elf1, string elf2, string elf3)
        {
            char commonChar = ' ';

            for (int i = 0; i < elf1.Length; i++)
            {
                if (elf2.Contains(elf1[i]) && elf3.Contains(elf1[i]))
                {
                    commonChar = elf1[i];
                    break;
                }
            }

            return commonChar;
        }

        private int GetCharValue(char commonChar)
        {
            for (int i = 0; i < itemValues.Length; i++)
            {
                if (commonChar == itemValues[i])
                {
                    return (i + 1);
                }
            }

            return 0;
        }

        private char GetCommonChar(string compartment1, string compartment2)
        {
            char commonChar = ' ';

            for (int i = 0; i < compartment1.Length; i++)
            {
                for (int j = 0; j < compartment2.Length; j++)
                {
                    if (compartment1[i] == compartment2[j])
                    {
                        commonChar = compartment1[i];
                        break;
                    }
                }
            }

            return commonChar;
        }
    }
}
