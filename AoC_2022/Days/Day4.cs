﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2022.Days
{
    public class Day4
    {
        private readonly ReadFromFile fileReader = new();

        public void CampCleanup()
        {
            string[] input = fileReader.GetTextLines(PathWays.TwentyTwo + "Day4Input.txt");
            int completeOverlaps = 0;
            int rangeOverlaps = 0;

            foreach (string line in input)
            {
                bool doesContain = false;
                bool doesOverlap = false;
                string[] assignments = line.Split(',');

                int[] range1 = ParseRange(assignments[0]);
                int[] range2 = ParseRange(assignments[1]);

                if (CheckContainment(range1, range2) || CheckContainment(range2, range1)) doesContain = true;
                if (CheckOverlap(range1, range2) || CheckOverlap(range2, range1)) doesOverlap = true;  

                if (doesContain) completeOverlaps++;
                if (doesOverlap) rangeOverlaps++;
            }

            Console.WriteLine("The amount of complete overlaps is: " + completeOverlaps);
            Console.WriteLine("The amount of overlaps is: " + rangeOverlaps);
        }

        private bool CheckOverlap(int[] range1, int[] range2)
        {
            bool doesOverlap = false;

            if (range1[0] >= range2[0] && range1[0] <= range2[1])
            {
                if (range1[1] <= range2[1] && range1[1] >= range2[0])
                {
                    doesOverlap = true;
                }
            }
            else if (range1[0] >= range2[0] && range1[0] <= range2[1])
            {
                doesOverlap = true;
            }
            else if (range1[1] <= range2[1] && range1[1] >= range2[0])
            {
                doesOverlap = true;
            }

            return doesOverlap;
        }

        private bool CheckContainment(int[] container, int[] content)
        {
            bool doesContain = false;

            if (container[0] <= content[0] && container[1] >= content[1])
            {
                doesContain = true;
            }

            return doesContain;
        }

        private int[] ParseRange(string assignment)
        {
            int[] result = new int[2];
            string[] range = assignment.Split('-');

            result[0] = int.Parse(range[0]);
            result[1] = int.Parse(range[1]);

            return result;
        }
    }
}
