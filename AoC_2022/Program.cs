﻿using AoC_2022.Days;

bool keepAlive = true;

Console.WriteLine("Hello, Bejoynd! \n Welcome to \n");


while (keepAlive)
{
    Console.ForegroundColor = ConsoleColor.Red;
    Console.WriteLine("       _       _                 _            __    ____          _      \r\n      / \\   __| |_   _____ _ __ | |_    ___  / _|  / ___|___   __| | ___ \r\n     / _ \\ / _` \\ \\ / / _ \\ '_ \\| __|  / _ \\| |_  | |   / _ \\ / _` |/ _ \\\r\n    / ___ \\ (_| |\\ V /  __/ | | | |_  | (_) |  _| | |__| (_) | (_| |  __/\r\n   /_/   \\_\\__,_| \\_/ \\___|_| |_|\\__|  \\___/|_|    \\____\\___/ \\__,_|\\___|");
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(" 222222222222222         000000000      222222222222222     222222222222222    \r\n2:::::::::::::::22     00:::::::::00   2:::::::::::::::22  2:::::::::::::::22  \r\n2::::::222222:::::2  00:::::::::::::00 2::::::222222:::::2 2::::::222222:::::2 \r\n2222222     2:::::2 0:::::::000:::::::02222222     2:::::2 2222222     2:::::2 \r\n            2:::::2 0::::::0   0::::::0            2:::::2             2:::::2 \r\n            2:::::2 0:::::0     0:::::0            2:::::2             2:::::2 \r\n         2222::::2  0:::::0     0:::::0         2222::::2           2222::::2  \r\n    22222::::::22   0:::::0 000 0:::::0    22222::::::22       22222::::::22   \r\n  22::::::::222     0:::::0 000 0:::::0  22::::::::222       22::::::::222     \r\n 2:::::22222        0:::::0     0:::::0 2:::::22222         2:::::22222        \r\n2:::::2             0:::::0     0:::::02:::::2             2:::::2             \r\n2:::::2             0::::::0   0::::::02:::::2             2:::::2             \r\n2:::::2       2222220:::::::000:::::::02:::::2       2222222:::::2       222222\r\n2::::::2222222:::::2 00:::::::::::::00 2::::::2222222:::::22::::::2222222:::::2\r\n2::::::::::::::::::2   00:::::::::00   2::::::::::::::::::22::::::::::::::::::2\r\n22222222222222222222     000000000     2222222222222222222222222222222222222222");
    Console.ForegroundColor = ConsoleColor.White;
    Console.WriteLine("\n Enter your choice of day: (0 to quit)");
    var input = Console.ReadLine();

    switch (input)
    {
        case "1":
            Day1 day1 = new();
            day1.CalorieCounting();
            break;
        case "2":
            Day2 day2 = new();
            day2.RockPaperScissor();
            break;
        case "3":
            Day3 day3 = new();
            day3.RucksackReorganization();
            break;
        case "4":
            Day4 day4 = new();
            day4.CampCleanup();
            break;
        case "5":
            Day5 day5 = new();
            day5.SupplyStacks(true);
            day5.SupplyStacks(false);
            break;
        case "6":
            Day6 day6 = new();
            day6.TuningTrouble();
            break;
        case "7":
            Day7 day7 = new();
            day7.SpaceSaver();
            break;
        case "8":
            Day8 day8 = new();
            day8.TreeTopHouse();
            break;
        case "9":
            Day9 day9 = new();
            day9.RopeSim();
            break;
        case "0":
            keepAlive = false;
            break;
        default:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Not a valid input, Please try again");
            Console.ForegroundColor = ConsoleColor.White;
            break;
    }

    Console.WriteLine("Press any key to continue...");
    Console.ReadKey();
    Console.Clear();
}
