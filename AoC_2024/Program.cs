﻿


using AoC_2024.Days;

bool keepAlive = true;
Console.Clear();
Console.WriteLine("Hello, Bejoynd! \n\n\n Welcome to \n");


while (keepAlive)
{
    Console.ForegroundColor = ConsoleColor.Red;

    Console.WriteLine("    ___       __                 __           ____  ______          __   \r\n   /   | ____/ /   _____  ____  / /_   ____  / __/ / ____/___  ____/ /__ \r\n  / /| |/ __  / | / / _ \\/ __ \\/ __/  / __ \\/ /_  / /   / __ \\/ __  / _ \\\r\n / ___ / /_/ /| |/ /  __/ / / / /_   / /_/ / __/ / /___/ /_/ / /_/ /  __/\r\n/_/  |_\\__,_/ |___/\\___/_/ /_/\\__/   \\____/_/    \\____/\\____/\\__,_/\\___/ ");
    Console.ForegroundColor = ConsoleColor.Green;
    Console.WriteLine(" ad888888b,    ,a888a,     ad888888b,        a8  \r\nd8\"     \"88  ,8P\"' `\"Y8,  d8\"     \"88      ,d88  \r\n         88 ,8P       Y8,          88     a8P88  \r\n        d8P 88         88         d8P   ,d8\" 88  \r\n       a8P  88         88        a8P   a8P'  88  \r\n     ,d8P   88         88      ,d8P  ,d8\"    88  \r\n   ,d8P'    88         88    ,d8P'   888888888888\r\n ,d8P'      `8b       d8'  ,d8P'             88  \r\na88\"         `8ba, ,ad8'  a88\"               88  \r\n88888888888    \"Y888P\"    88888888888        88  \r\n                                                 \r\n                                                 \r\n                                                 ");
    Console.ForegroundColor = ConsoleColor.White;
    Console.WriteLine("\n\n\n\n Enter your choice of day: (0 to quit)");
    var input = Console.ReadLine();

    switch (input)
    {
        case "0":
            keepAlive = false;
            break;
        case "1":
            var day1 = new Day1();
            day1.HistorianHysteria();
            break;
        case "2":
            var day2 = new Day2();
            day2.RedNosedReport();
            break;
        case "3":
            var day3 = new Day3();
            day3.MullItOver();
            break;
        case "4":
            var day4 = new Day4();
            day4.CeresSearch();
            break;
        case "5":
            var day5 = new Day5();
            day5.PrintQueue();
            break;
        case "6":
            var day6 = new Day6();
            day6.GuardGallivant2D();
            break;
        case "7":
            var day7 = new Day7();
            day7.BridgeRepair();
            break;
        default:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(@"────────█████─────────────█████
────████████████───────████████████
──████▓▓▓▓▓▓▓▓▓▓██───███▓▓▓▓▓▓▓▓▓████
─███▓▓▓▓▓▓▓▓▓▓▓▓▓██─██▓▓▓▓▓▓▓▓▓▓▓▓▓███
███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███
██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██
██▓▓▓▓▓▓▓▓▓──────────────────▓▓▓▓▓▓▓▓██
██▓▓▓▓▓▓▓─██───████─█──█─█████─▓▓▓▓▓▓██
██▓▓▓▓▓▓▓─██───█──█─█──█─██────▓▓▓▓▓▓██
███▓▓▓▓▓▓─██───█──█─█──█─█████─▓▓▓▓▓▓██
███▓▓▓▓▓▓─██───█──█─█──█─██────▓▓▓▓▓▓██
─███▓▓▓▓▓─████─████─████─█████─▓▓▓▓███
───███▓▓▓▓▓──────────────────▓▓▓▓▓▓███
────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████
─────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████
───────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████
──────────████▓▓▓▓▓▓▓▓▓▓▓▓████
─────────────███▓▓▓▓▓▓▓████
───────────────███▓▓▓███
─────────────────██▓██
──────────────────███
────────█████─────────────█████
────████████████───────████████████
──████▓▓▓▓▓▓▓▓▓▓██───███▓▓▓▓▓▓▓▓▓████
─███▓▓▓▓▓▓▓▓▓▓▓▓▓██─██▓▓▓▓▓▓▓▓▓▓▓▓▓███
███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███
██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██
██▓▓▓▓▓▓▓────────────────────▓▓▓▓▓▓▓▓██
██▓▓▓▓▓───████▄─███─████─█████──▓▓▓▓▓██
███▓▓▓▓───██──█──█──█────█──────▓▓▓▓███
███▓▓▓▓───██──█──█──████─█████──▓▓▓▓███
─███▓▓▓───██──█──█──█────────█──▓▓▓▓██
──████▓───████▀─███─████─█████──▓████
───███▓▓───────────────────── ▓▓▓███
────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████
─────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████
───────████▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓████
──────────████▓▓▓▓▓▓▓▓▓▓▓████
─────────────███▓▓▓▓▓▓▓███
───────────────███▓▓▓███
─────────────────██▓██
──────────────────███");
            Console.WriteLine("Not a valid input, Please try again");
            Console.ForegroundColor = ConsoleColor.White;
            break;
    }

    Console.WriteLine("Press any key to continue...");
    Console.ReadKey();
    Console.Clear();
}
