﻿
using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text;

namespace AoC_2024.Days;
public sealed class Day5
{
    private readonly string[] input = ReadFromFileStatic.GetTextLines(PathWays.TwentyFour + "d5.txt");

    public void PrintQueue()
    {
        var rules = input.Where(x => x.Contains("|")).ToList();
        var lines = input.Where(x => !x.Contains("|") && !string.IsNullOrEmpty(x)).ToList();
        var correctLinesCount = 0;
        var correctLineMiddleValue = new List<int>();
        var correctedLineMiddlevalues = new List<int>();

        for (int line = 0; line < lines.Count; line++)
        {
            var lineIsCorrect = true;
            var currLine = lines[line].Split(",").ToList();

            for (int i = 0; i < rules.Count; i++)
            {
                var ruleParts = rules[i].Split("|");
                if (currLine.Contains(ruleParts[0]) && currLine.Contains(ruleParts[1]) && currLine.IndexOf(ruleParts[0]) > currLine.IndexOf(ruleParts[1]))
                {
                    lineIsCorrect = false;
                }
            }

            if (lineIsCorrect)
            {
                correctLinesCount++;

                correctLineMiddleValue.Add(int.Parse(currLine[currLine.Count / 2]));
            }
            else 
            {
                correctedLineMiddlevalues.Add(CorrectAndGetMiddleValue(currLine, rules));

            }
        }

        Console.WriteLine("Part 1: " + correctLineMiddleValue.Sum());
        Console.WriteLine("Part 2: " + correctedLineMiddlevalues.Sum());
    }

    private static int CorrectAndGetMiddleValue(List<string> currLine, List<string> rules)
    {
        bool modified;
        do
        {
            modified = false;
            foreach (string rule in rules)
            {
                var parts = rule.Split('|');

                var firstValue = parts[0];
                var secondValue = parts[1];

                var firstIndex = currLine.IndexOf(firstValue);
                var secondIndex = currLine.IndexOf(secondValue);

                if (firstIndex > secondIndex && firstIndex != -1 && secondIndex != -1)
                {
                    Swap(currLine, firstIndex, secondIndex);
                    modified = true;
                }
            }
        } while (modified);

        return int.Parse(currLine[currLine.Count / 2]);
    }

    static void Swap(List<string> sb, int index1, int index2)
    {
        var temp = sb[index1];
        sb[index1] = sb[index2];
        sb[index2] = temp;
    }
}

