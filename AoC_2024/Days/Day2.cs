﻿
using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2024.Days;
public sealed class Day2
{
    public void RedNosedReport() 
    {
        var input = ReadFromFileStatic.GetTextLines(PathWays.TwentyFour + "d2.txt");

        var safeReports = 0;
        var safeReportsPart2 = 0;


        for (int i = 0; i < input.Length; i++)
        {
            var intRow = new List<int>();   
            var splitted = input[i].Split(' ', StringSplitOptions.RemoveEmptyEntries);
            foreach (var num in splitted)
            {
                intRow.Add(int.Parse(num.ToString()));
            }

            if (IsSafe(intRow.ToArray())) safeReports++;
            if(Part2(intRow)) safeReportsPart2++;
        }

        Console.WriteLine("Part 1: " + safeReports);
        Console.WriteLine("Part 2: " + safeReportsPart2);

    }

    private bool Part2(List<int> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            var adjustedList = new List<int>(list);
            adjustedList.RemoveAt(i);
            if (IsSafe(adjustedList.ToArray())) return true;
        }

        return false;
    }

    private static bool IsSafe(int[] array)
    {
        bool increasing = true;
        bool decreasing = true;

        for (int i = 1; i < array.Length; i++)
        {
            if (HasLargeDifference(array)) return false;
            if (array[i] > array[i - 1]) decreasing = false;
            else if (array[i] < array[i - 1]) increasing = false;
            else return false;

            if (!increasing && !decreasing) return false;
        }

        return true;
    }

    private static bool HasLargeDifference(int[] array)
    {
        for (int i = 1; i < array.Length; i++)
        {
            if (Math.Abs(array[i] - array[i - 1]) > 3)
            {
                return true; 
            }
        }

        return false;
    }
}
