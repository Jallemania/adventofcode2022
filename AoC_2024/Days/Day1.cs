﻿using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2024.Days;
public sealed class Day1
{
    public void HistorianHysteria()
    {
        var input = ReadFromFileStatic.GetText(PathWays.TwentyFour + "d1.txt");
        var index = 0;

        var leftList = new List<string>();
        var rightList = new List<string>();
        var orgLeftList = new List<int>();
        var orgRightList = new List<int>();

        foreach (var num in input.Split(new[] { ' ', '\r', '\n' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries ))
        {
            if (index % 2 == 0) { leftList.Add(num); orgLeftList.Add(int.Parse(num)); }
            else { rightList.Add(num); orgRightList.Add(int.Parse(num)); }

            index++;
        }

        leftList.Sort();
        rightList.Sort();

        var diffs = new List<int>();

        for (int i = 0; i < leftList.Count; i++)
        {
            var left = int.Parse(leftList[i]);
            var right = int.Parse(rightList[i]);
            int diff;

            if (left > right) diff = left - right;
            else diff = right - left;
            diffs.Add(diff);
        }

        var sum = diffs.Sum();
        Console.WriteLine("Part 1: " + sum);

        Part2(orgLeftList, orgRightList);
    }

    public void Part2(List<int> leftList, List<int> rightList)
    {
        var probabilities = new List<int>();

        for (int i = 0; i < leftList.Count; i++)
        {
            var left = leftList[i];
            var rightOccurances = rightList.FindAll(x => x == left);

            probabilities.Add(left * rightOccurances.Count);

        }

        Console.WriteLine("Part 2: " + probabilities.Sum());
    }

}
