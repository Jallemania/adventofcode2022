﻿
using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2024.Days;
public sealed class Day4
{
    private static readonly string[] input = ReadFromFileStatic.GetTextLines(PathWays.TwentyFour + "d4.txt");
    private string wordToFind = "XMAS";
    private readonly int rows = input.Length;
    private readonly int cols = input[0].Length;

    public void CeresSearch()
    {
        Console.WriteLine("Part 1: " + CountWords());
        wordToFind = "MAS";
        Console.WriteLine("Part 2: " + CountCrossMAS());
    }

    private int CountWords()
    {
        var count = 0;

        int[] rowDirections = { -1, -1, -1, 0, 1, 1, 1, 0 };
        int[] colDirections = { -1, 0, 1, 1, 1, 0, -1, -1 };

        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                if (input[row][col] == wordToFind[0])
                {
                    for (int direction = 0; direction < 8; direction++)
                    {
                        if (IsWordFound(wordToFind, row, col, rowDirections[direction], colDirections[direction]))
                        {
                            count++;
                        }
                    }
                }
            }
        }

        return count;
    }

    private int CountCrossMAS()
    {
        var count = 0;

        string reversedWord = new string(wordToFind.Reverse().ToArray());

        for (int row = 0; row < rows; row++)
        {
            for (int col = 0; col < cols; col++)
            {
                if (input[row][col] == wordToFind[1])
                {
                    bool topLeftBottomRight = 
                        (IsWordFound(wordToFind, row, col, -1, -1) && IsWordFound(reversedWord, row, col, 1, 1)) ||
                        (IsWordFound(reversedWord, row, col, -1, -1) && IsWordFound(wordToFind, row, col, 1, 1));

                    bool topRightBottomLeft =
                        (IsWordFound(wordToFind, row, col, -1, 1) && IsWordFound(reversedWord, row, col, 1, -1)) ||
                        (IsWordFound(reversedWord, row, col, -1, 1) && IsWordFound(wordToFind, row, col, 1, -1));


                    bool isCrossValid = topLeftBottomRight &&
                    topRightBottomLeft;

                    if (isCrossValid)
                    {
                        count++;
                        Console.WriteLine($"Cross found at ({row}, {col})");
                    }
                }
            }
        }

        return count;
    }

    private bool IsWordFound(string word, int startRow, int startCol, int rowDir, int colDir)
    {
        for (int i = 0; i < word.Length; i++)
        {
            int newRow = GetPos(word != "XMAS", startRow, i, rowDir);
            int newCol = GetPos(word != "XMAS", startCol, i, colDir);

            if (!IsValidPosition(newRow, newCol))
            {
                return false;
            }

            if (input[newRow][newCol] != word[i])
            {
                return false;
            }
        }

        return true;
    }


    private static int GetPos(bool isCenter, int start, int i, int dir)
    {
        if (isCenter)
        {
            return start + (i - 1) * dir; // Center-based adjustment for "MAS"
        }
        else
        {
            return start + i * dir; // Normal movement
        }
    }

    private bool IsValidPosition(int row, int col)
        => row >= 0 && row < rows && col >= 0 && col < cols;

}
