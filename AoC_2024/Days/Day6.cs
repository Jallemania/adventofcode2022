﻿
using AoC_Common.Constants;
using AoC_Common.Services;

namespace AoC_2024.Days;

public class Day6
{

    private readonly char[,] _2dInput = ReadFromFileStatic.GetTextAs2dArray(PathWays.TwentyFour + "d6.txt");

    public void GuardGallivant2D()
    {
        (int startX, int startY) = GetStartPos(_2dInput);
        var inputCopy = _2dInput.Clone() as char[,];
        var looped = false;

        inputCopy = StartGuardMovement(inputCopy!, startX, startY, ref looped);

        var occurances = CountDirOccurrences(inputCopy);

        Console.WriteLine("Part 1: " + occurances);

        for (int x = 0; x < inputCopy.GetLength(0); x++)
        {
            for (int y = 0; y < inputCopy.GetLength(1); y++)
            {
                if (IsDir(inputCopy[x, y])) { Console.ForegroundColor = ConsoleColor.Red; }
                else if (inputCopy[x, y] == '#') Console.ForegroundColor = ConsoleColor.Green;
                else Console.ForegroundColor = ConsoleColor.White;
                Console.Write(inputCopy[x, y]);
            }

            Console.Write("\r\n");
        }

        Part2();
    }

    public void Part2()
    {
        (int startX, int startY) = GetStartPos(_2dInput);
        var cleanCopy = _2dInput.Clone() as char[,];
        var possiblePositions = 0;


        var possibilities = new List<(int row, int col)>();
        for (var row = 0; row < cleanCopy!.GetLength(0); row++)
        {
            for (var col = 0; col < cleanCopy.GetLength(1); col++)
            {
                if (cleanCopy[row, col] == '.') possibilities.Add((row, col));
            }
        }

        foreach (var possibility in possibilities)
        {
            var looped = false;
            cleanCopy![possibility.row, possibility.col] = '#';
            StartGuardMovement(cleanCopy, startX, startY, ref looped);
            if (looped) possiblePositions++;

            cleanCopy = _2dInput.Clone() as char[,];
        }

        Console.WriteLine("Part 2: " + possiblePositions);
    }

    static (int, int) GetStartPos(char[,] input)
    {
        for (int x = 0; x < input.GetLength(0); x++)
        {
            for (int y = 0; y < input.GetLength(1); y++)
            {
                if (input[x, y] == '^')
                {
                    return (x, y);
                }
            }
        }

        return (0, 0);
    }

    static char[,] StartGuardMovement(char[,] inputCopy, int startX, int startY, ref bool looped)
    {
        var loopCounter = 0;
        var itr = 0;
        bool isOut;
        do
        {
            if (loopCounter > 1 || itr > 9999)
            {
                looped = true;
                return inputCopy;
            }

            isOut = IsOutOfBounds(startX, startY, inputCopy);

            if (!isOut)
            {
                switch (inputCopy[startX, startY])
                {
                    case '^':
                        if (Move(ref startX, ref startY, startX - 1, startY, ref inputCopy, '^', '>')) loopCounter++;
                        break;
                    case '>':
                        if (Move(ref startX, ref startY, startX, startY + 1, ref inputCopy, '>', 'v')) loopCounter++;
                        break;
                    case 'v':
                        if (Move(ref startX, ref startY, startX + 1, startY, ref inputCopy, 'v', '<')) loopCounter++;
                        break;
                    case '<':
                        if (Move(ref startX, ref startY, startX, startY - 1, ref inputCopy, '<', '^')) loopCounter++;
                        break;
                    default:
                        Console.WriteLine("lost the direction...");
                        return inputCopy;
                }
            }
            itr++;
        }
        while (!isOut);

        return inputCopy;
    }

    static int CountDirOccurrences(char[,] array)
    {
        int count = 0;

        for (int i = 0; i < array.GetLength(0); i++)
        {
            for (int j = 0; j < array.GetLength(1); j++)
            {
                if (IsDir(array[i, j]))
                {
                    count++;
                }
            }
        }

        return count;
    }


    static bool Move(ref int startRow, ref int startCol, int nextPosRow, int nextPosCol, ref char[,] _2dInput, char direction, char directionChange)
    {
        if (_2dInput[nextPosRow, nextPosCol] == '.' || IsDir(_2dInput[nextPosRow, nextPosCol]))
        {
            if (_2dInput[nextPosRow, nextPosCol] == direction) return true;
            _2dInput[nextPosRow, nextPosCol] = direction;
            startRow = nextPosRow;
            startCol = nextPosCol;
            return false;
        }
        else
        {
            _2dInput[startRow, startCol] = directionChange;
            return false;
        }
    }

    static bool IsOutOfBounds(int x, int y, char[,] grid)
        => x <= 0 || x >= grid.GetLength(0) - 1 || y <= 0 || y >= grid.GetLength(1) - 1;

    static bool IsDir(char ch)
        => ch == '^' || ch == '>' || ch == 'v' || ch == '<';
}
