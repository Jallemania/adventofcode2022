﻿using AoC_Common.Constants;
using AoC_Common.Services;
using System.Numerics;

namespace AoC_2024.Days;

public class Day7
{
    private readonly string[] input = ReadFromFileStatic.GetTextLines(PathWays.TwentyFour + "d7.txt");

    public void BridgeRepair()
    {
        var sums = new List<BigInteger>();
        var sums2 = new List<BigInteger>();

        foreach (var calculation in input)
        {
            var parts = calculation.Split(':');
            var sum = BigInteger.Parse(parts[0]);
            var numbers = parts[1].Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(BigInteger.Parse).ToList();

            sums.Add(FindPossibleSolutions(sum, numbers));
            sums2.Add(FindPossibleSolutionsP2(sum, numbers));
        }

        Console.WriteLine("Part 1:" + sums.Aggregate(BigInteger.Zero, (acc, val) => acc + val));
        Console.WriteLine("Part 2:" + sums2.Aggregate(BigInteger.Zero, (acc, val) => acc + val));
    }

    private BigInteger FindPossibleSolutions(BigInteger targetSum, List<BigInteger> numbers)
    {
        int n = numbers.Count;

        int totalCombinations = (int)Math.Pow(2, n - 1);
        for (int i = 0; i < totalCombinations; i++)
        {
            BigInteger currentValue = numbers[0];

            for (int j = 0; j < n - 1; j++)
            {
                bool isMultiply = (i & (1 << j)) != 0;

                if (isMultiply)
                {
                    currentValue *= numbers[j + 1];
                }
                else
                {
                    currentValue += numbers[j + 1];
                }
            }

            if (currentValue == targetSum) return targetSum;
        }

        return 0; // Return 0 if no combinations match
    }

    private BigInteger FindPossibleSolutionsP2(BigInteger targetSum, List<BigInteger> numbers)
    {
        int n = numbers.Count;

        int totalCombinations = (int)Math.Pow(3, n - 1); // 3^(n-1) combinations (+, *, || between n numbers)

        for (int i = 0; i < totalCombinations; i++)
        {
            BigInteger currentValue = numbers[0];
            int operatorCode = i; 

            for (int j = 0; j < n - 1; j++)
            {
                int op = operatorCode % 3; // 0 = '+', 1 = '*', 2 = '||'
                operatorCode /= 3;

                if (op == 0)
                {
                    currentValue += numbers[j + 1];
                }
                else if (op == 1)
                {
                    currentValue *= numbers[j + 1];
                }
                else if (op == 2)
                {
                    currentValue = Concatenate(currentValue, numbers[j + 1]);
                }
            }

            if (currentValue == targetSum) return targetSum;
            
        }

        return 0; // Return 0 if no combinations match
    }

    static BigInteger Concatenate(BigInteger left, BigInteger right)
        => BigInteger.Parse(left.ToString() + right.ToString());

}
