﻿

using AoC_Common.Constants;
using AoC_Common.Services;
using System.Text.RegularExpressions;

namespace AoC_2024.Days;
public sealed class Day3
{
    private readonly string _input = ReadFromFileStatic.GetText(PathWays.TwentyFour + "d3.txt").Trim();
    private const string pattern = @"mul\((\d+), (\d+)\)";
    private const string pattern2 = @"mul\((\d+), (\d+)\)|do\(\)|don't\(\)";

    public void MullItOver()
    {
        var results = new List<int>();
        var matches = Regex.Matches(_input, pattern, RegexOptions.IgnorePatternWhitespace);

        foreach (Match match in matches)
        {
            results.Add(Mul(int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value)));
        }

        Console.WriteLine("Part 1: " + results.Sum());
        MullItOver2();

    }

    private void MullItOver2() 
    {
        var results = new List<int>();
        var matches = Regex.Matches(_input, pattern2, RegexOptions.IgnorePatternWhitespace);
        var dont = false;

        foreach (Match match in matches)
        {
            if (match.Value == "do()" || match.Value == "don't()") { dont = match.Value == "don't()"; continue; }
            if (dont) continue; 
            results.Add(Mul(int.Parse(match.Groups[1].Value), int.Parse(match.Groups[2].Value)));
        }

        Console.WriteLine("Part 2: " + results.Sum());

    }

    private int Mul(int x, int y)
        => x * y;
}
